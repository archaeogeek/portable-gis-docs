====================================
Apps that require minor modification
====================================

Geoserver
---------

Download the Platform Independent Binary (version 2.15.1) from 'geoserver.org'_ and extract the contents into the apps folder, in a directory called geoserver. Copy the contents of the ``geoserver`` directory from the `Portable GIS repository`_ into the same folder, overwriting as necessary.


Python 2.7
----------

Download Python 2.7 from `Portable Python`_ and install it into ``apps\python27``.  Copy the contents of the ``python27`` directory from the `Portable GIS repository`_ into the same folder, overwriting as necessary.

(Note that Python 3.7 is included in ms4w below).


ms4w
----

Download the latest zip file of ms4w from `ms4w.com`_. Extract the ``ms4w`` folder from within the download and place it in the ``apps`` folder of your Portable GIS installation. Copy the contents of the ``ms4w`` directory from the `Portable GIS repository`_ into the same folder, overwriting as necessary.

.. _Portable Python: http://portablepython.com/
.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis
.. _geoserver.org: http://geoserver.org/release/stable/
.. _ms4w.com: http://ms4w.com
