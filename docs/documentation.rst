=============
Documentation
=============

The Portable GIS user documentation from this repository should be included in the final installation. Convert ``user_docs.rst`` to a pdf using something like `Cloud Convert <https://cloudconvert.com/rst-to-pdf>`_ and save it as ``pgis_docs.pdf`` in the ``usbgis\apps\docs`` folder of your Portable GIS installation. 

**To ensure it is linked to correctly from the menu, it must be named pgis_docs.pdf**.

The ``docs`` folder also contains the changelog and license text files (.txt) also linked to from the menu.

The readme file linked to from the installer should be placed in the root of your Portable GIS installation and named **portablegis_readme.txt**.

.. code::

	|-- raw_docs
		|-- pgis_docs.rst
	|-- usbgis
		|-- apps
		|-- data
		|-- docs
			|-- changelog.txt
			|-- license.txt
			|-- pgis_docs.pdf
	|-- portablegis_readme.txt
	|-- ...

.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis