=================================
Apps that require a bit more work
=================================

PostgreSQL and PostGIS
----------------------

PostgreSQL
-----------

Download the zip file (32bit) for PostgreSQL 10.7 from `postgresql.org`_ (the section for "Advanced Users").

Extract the ``pgsql`` folder from the zip file and copy it to the apps folder.

The following credentials are assumed throughout the documentation and settings:

PostgreSQL is set up with the following options: 

 * Username: pgis
 * Password: pgis
 * Port: 3432
 * Host: localhost or 127.0.0.1
 * Spatially enabled template database: template_postgis

Create a user on your pc called ``pgis`` with the password ``pgis`` and open a command prompt as that user. Change directory to the ``apps\pgsql\bin`` directory and initialise the db in the following manner:

``> initdb.exe --pgdata=..\..\..\PGDATA --username=pgis --locale=english_UK --encoding=UTF8``

Copy the files from the ``pgsql`` folder in the `Portable GIS repository`_ into your ``pgsql`` folder, overwriting as necessary.

(At this point you should probably transfer your build to an actual USB stick so you can start postgresql using the batch file start_pgsql.bat and deal with any error messages).

Note that there's a pgpass.conf file in ``apps\pgsql`` which you should edit if you change the default credentials. This is utilised by setting the PGPASSFILE environment variable when starting postgresql.

pgAdmin 4
---------

PgAdmin 4 is configured to run with a fixed port of 5436 and in the provided version of firefox. To change the port number, edit ``pgsql\pgAdmin 4\web\config_local.py`` and change the value of ``DEFAULT_SERVER_PORT``.

PostGIS
--------

Download the zip file (currently the 2.5 version) from `download.osgeo.org`_ and extract the contents into the pgsql directory, overwriting if needed.

Create a new database (called, eg template_postgis) and in a sql window run ``create extension postgis``

To make it a template execute the following:

``UPDATE pg_database SET datistemplate = true WHERE datname = 'template_postgis';
GRANT ALL ON geometry_columns TO PUBLIC; 
GRANT ALL ON spatial_ref_sys TO PUBLIC;``


QGIS
----

Download 32bit stand-alone qgis installer (version 3.4.5) from the `QGIS Download Site`_ and install it on a windows pc. Copy the ``C:\Program Files\QGIS 3.4`` folder  into the ``apps`` folder, renaming it ``qgis``, and then apply the files from the ``qgis`` folder in the `Portable GIS repository`_ to the same location, over-writing as necessary.

QGIS may inherit settings from other versions of qgis installed on your computer. If you have trouble starting it, then try deleting any ``*.env`` files from ``[path to usbgis folder]\apps\qgis\bin``.


Java 8 jdk
----------

Oracle's jdk8 is provided in the `Portable GIS repository`_ but if you wish to build your own follow the instructions below. These instructions originally came from `brucalipto.org`_ and are reproduced here in case that site is no longer available.

* Download a copy of the Windows 32bit (X86) jdk version 8 executable from `oracle`_
* Open the downloaded executable with 7-zip, and the extract the tools.zip file inside to a tools folder.
* At this point you should have the following folder structure ``jdk_8\tools``
* Open a command prompt inside the tools directory and run the following command, substituting the correct drive letter::

	> for /r %x in (*.pack) do C:\jdk_8\tools\bin\unpack200 "%x" "%x.jar"

* Find and rename all the .pack.jar files present in the tools folder and subfolders::

	> dir /B /S *.pack.jar
	C:\jdk_8\tools\jre\lib\charsets.pack.jar
	C:\jdk_8\tools\jre\lib\deploy.pack.jar
	C:\jdk_8\tools\jre\lib\javaws.pack.jar
	C:\jdk_8\tools\jre\lib\jsse.pack.jar
	C:\jdk_8\tools\jre\lib\plugin.pack.jar
	C:\jdk_8\tools\jre\lib\rt.pack.jar
	C:\jdk_8\tools\jre\lib\ext\jfxrt.pack.jar
	C:\jdk_8\tools\jre\lib\ext\localedata.pack.jar
	C:\jdk_8\tools\lib\tools.pack.jar

	> ren C:\jdk_8\tools\jre\lib\charsets.pack.jar charsets.jar
	> ren C:\jdk_8\tools\jre\lib\deploy.pack.jar deploy.jar
	> ren C:\jdk_8\tools\jre\lib\javaws.pack.jar javaws.jar
	> ren C:\jdk_8\tools\jre\lib\jsse.pack.jar jsse.jar
	> ren C:\jdk_8\tools\jre\lib\plugin.pack.jar plugin.jar
	> ren C:\jdk_8\tools\jre\lib\rt.pack.jar rt.jar
	> ren C:\jdk_8\tools\jre\lib\ext\jfxrt.pack.jar jfxrt.jar
	> ren C:\jdk_8\tools\jre\lib\ext\localedata.pack.jar localedata.jar
	> ren C:\jdk_8\tools\lib\tools.pack.jar tools.jar

* Test this has worked OK::

	C:\jdk_8\tools\bin\java -version
	java version "1.8.0-ea"
	Java(TM) SE Runtime Environment (build 1.8.0-ea-b121)
	Java HotSpot(TM) Client VM (build 25.0-b63, mixed mode)

.. _brucalipto.org: http://www.brucalipto.org/java/how-to-create-a-portable-jdk-1-dot-8-on-windows
.. _oracle: http://www.oracle.com/technetwork/java/javase/overview/index.html
.. _postgresql.org: http://www.postgresql.org/download/windows/
.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis
.. _download.osgeo.org: http://download.osgeo.org/postgis/windows/pg93/
.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis
.. _download.osgeo.org: http://download.osgeo.org/postgis/windows/pg93/
.. _QGIS Download Site: http://qgis.org/en/site/forusers/download.html
