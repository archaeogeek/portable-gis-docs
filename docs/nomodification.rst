==============================================
Apps that can be included without modification
==============================================

Loader
------

Download a zip file of Loader from `github.com/AstunTechnology/Loader`_. Place the ``Loader`` folder into the ``apps`` folder in your Portable GIS directory.

.. _github.com/AstunTechnology/Loader: https://github.com/AstunTechnology/Loader
