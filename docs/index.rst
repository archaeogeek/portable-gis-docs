.. Portable GIS documentation master file, created by
   sphinx-quickstart on Tue Mar  8 17:53:35 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation for Portable GIS
=============================================

Portable GIS is a set of open source GIS programs designed to be run from a USB stick, in windows, with no installation or configuration. It's hosted at `portablegis.xyz`_, where you will also find details of the latest versions of the packages included. This documentation is split into two sections:

* **User Documentation**: Using Portable GIS
* **Developer Documentation**: Setting up your own build environment for Portable GIS, from the repository at `gitlab.com/archaeogeek/portable-gis`_. Note that currently there are only instructions for building a development environment in windows.

Problems?
---------

* For problems with the documentation, please submit an issue at `gitlab.com/archaeogeek/portable-gis-docs/issues`_
* For problems with the source code then please submit an issue at `gitlab.com/archaeogeek/portable-gis/issues`_.

.. _portablegis.xyz: http://portablegis.xyz
.. _gitlab.com/archaeogeek/portable-gis: https://gitlab.com/archaeogeek/portable-gis
.. _gitlab.com/archaeogeek/portable-gis-docs/issues: https://gitlab.com/archaeogeek/portable-gis-docs/issues
.. _gitlab.com/archaeogeek/portable-gis/issues: https://gitlab.com/archaeogeek/portable-gis/issues

.. toctree::
	:maxdepth: 3
	:caption: User Documentation

	user_docs

.. toctree::
	:maxdepth: 3
	:caption: Developer Documentation

	buildenvironment

