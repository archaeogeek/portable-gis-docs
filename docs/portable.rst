======================================
Apps that require the portable version
======================================

Geany
-----

Download a copy of Portable Geany from `portableapps.com/apps/development/geany_portable`_ and place the executable into the ``apps/geany`` folder.


Firefox
-------

Download a copy of Portable Firefox from `portableapps.com/apps/internet/firefox_portable`_ and place the executable into the ``apps/firefox`` folder.



PDF Reader
----------

Download a copy of Portable Foxit Reader from `portableapps.com/apps/office/foxit_reader_portable`_ and place the executable into the ``apps/pdfreader`` folder.

.. _portableapps.com/apps/development/geany_portable: http://portableapps.com/apps/development/geany_portable
.. _portableapps.com/apps/internet/firefox_portable: http://portableapps.com/apps/internet/firefox_portable
.. _portableapps.com/apps/office/foxit_reader_portable: http://portableapps.com/apps/office/foxit_reader_portable