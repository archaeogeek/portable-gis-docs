PostgresSQL and PostGIS
=======================

UNTESTED

PostgreSQL
-----------


Download the zip file (32bit) for PostgreSQL 9.3 from `postgresql.org`_ (the section for "Advanced Users"). 

Extract this to the apps folder and rename it ``postgresql93``.

Create a user on your pc called pgis with the password pgis and open a command prompt as that user. Change directory to the apps\postgresql93\bin directory and initialise the db in the following manner:

``> initdb.exe --pgdata=..\PGDATA --username=pgis --locale=UTF8 --xlogdir=..\..\..\data\pg93_log``

Copy the files from the ``postgresql93`` folder in the `Portable GIS repository`_ into your ``postgresql93`` folder, overwriting as necessary.

(At this point you should probably transfer your build to an actual USB stick so you can start postgresql using the batch file start_pgsql.bat and deal with any error messages)

PostGIS
--------

Download the zip file (currently the 2.1 version) from `download.osgeo.org`_ and extract the contents into the postgresql93 directory, overwriting if needed.

Create a new template database (called, eg template_postgis) and run spatial_ref_sys.sql and postgis.sql on that database to make it spatial.


.. _postgresql.org: http://www.postgresql.org/download/windows/
.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis
.. _download.osgeo.org: http://download.osgeo.org/postgis/windows/pg93/
