Java 8 jdk
=================

Oracle's jdk8 is provided in the `Portable GIS repository`_ but if you wish to build your own follow the instructions below. These instructions originally came from `brucalipto.org`_ and are reproduced here in case that site is no longer available.

* Download a copy of the Windows 32bit (X86) jdk version 8 executable from `oracle`_
* Open the downloaded executable with 7-zip, and the extract the tools.zip file inside to a tools folder.
* At this point you should have the following folder structure ``jdk_8\tools``
* Open a command prompt inside the tools directory and run the following command, substituting the correct drive letter::

	> for /r %x in (*.pack) do C:\jdk_8\tools\bin\unpack200 "%x" "%x.jar"

* Find and rename all the .pack.jar files present in the tools folder and subfolders::

	> dir /B /S *.pack.jar
	C:\jdk_8\tools\jre\lib\charsets.pack.jar
	C:\jdk_8\tools\jre\lib\deploy.pack.jar
	C:\jdk_8\tools\jre\lib\javaws.pack.jar
	C:\jdk_8\tools\jre\lib\jsse.pack.jar
	C:\jdk_8\tools\jre\lib\plugin.pack.jar
	C:\jdk_8\tools\jre\lib\rt.pack.jar
	C:\jdk_8\tools\jre\lib\ext\jfxrt.pack.jar
	C:\jdk_8\tools\jre\lib\ext\localedata.pack.jar
	C:\jdk_8\tools\lib\tools.pack.jar

	> ren C:\jdk_8\tools\jre\lib\charsets.pack.jar charsets.jar
	> ren C:\jdk_8\tools\jre\lib\deploy.pack.jar deploy.jar
	> ren C:\jdk_8\tools\jre\lib\javaws.pack.jar javaws.jar
	> ren C:\jdk_8\tools\jre\lib\jsse.pack.jar jsse.jar
	> ren C:\jdk_8\tools\jre\lib\plugin.pack.jar plugin.jar
	> ren C:\jdk_8\tools\jre\lib\rt.pack.jar rt.jar
	> ren C:\jdk_8\tools\jre\lib\ext\jfxrt.pack.jar jfxrt.jar
	> ren C:\jdk_8\tools\jre\lib\ext\localedata.pack.jar localedata.jar
	> ren C:\jdk_8\tools\lib\tools.pack.jar tools.jar

* Test this has worked OK::

	C:\jdk_8\tools\bin\java -version
	java version "1.8.0-ea"
	Java(TM) SE Runtime Environment (build 1.8.0-ea-b121)
	Java HotSpot(TM) Client VM (build 25.0-b63, mixed mode)

.. _brucalipto.org: http://www.brucalipto.org/java/how-to-create-a-portable-jdk-1-dot-8-on-windows
.. _oracle: http://www.oracle.com/technetwork/java/javase/overview/index.html
.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis