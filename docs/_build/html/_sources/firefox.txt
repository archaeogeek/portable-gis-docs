Firefox
=======

Download a copy of Portable Firefox from `portableapps.com`_ and place the executable into the firefox folder.

.. _portableapps.com: http://portableapps.com/apps/internet/firefox_portable