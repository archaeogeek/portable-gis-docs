===============
Getting Started
===============

This document will show you how to set up the build environment for creating an edition of Portable GIS from the component software. Unfortunately at present it's windows only but if that changes, so much the better!

Required Programmes
-------------------

* Git
* AutoIT (to build menu)
* Universal Extractor (to extract executables)
* 7-zip (for when Universal Extractor doesn't work)
* NSIS (to build installer)

Folder Structure
----------------

.. code::

	|-- raw_docs
		|-- pgis_docs.odt
	|-- usbgis
		|-- apps
			|-- ...
		|-- data
			|-- ...
		|-- docs
			|-- changelog.txt
			|-- license.txt
			|-- pgis_docs.pdf
	|--your.ico
	|--your.bmp
	|--portablegis_readme.txt
	|--yourinstaller.nsi
	|--yourmenu.au3
	|--yourmenu.exe
	|--yourinstaller.exe

App Preparation
---------------

.. toctree::

	portable
	nomodification
	minor
	major

Menu and Install File
---------------------

.. toctree::

	menuandinstall

Documentation
-------------

.. toctree::
	
	documentation