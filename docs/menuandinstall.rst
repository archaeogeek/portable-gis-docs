==================
Menu and Installer
==================

Menu
----

The Portable GIS menu is built with AutoIT. Yes I know there are probably better things to use these days but if it ain't broke...

Download `AutoIT`_ and install on your local machine.

The Portable GIS menu source file (portablegis_xxx.au3) can be found in the `Portable GIS repository`_ and should be placed in the root of your Portable GIS installation, like so:

.. code::

	|-- raw_docs
	|-- usbgis
		|-- apps
		|-- data
		|-- docs
	|-- your.ico
	|-- your.bmp
	|-- your.jpg
	|-- portablegis_readme.txt
	|-- yourinstaller.nsi
	|-- yourmenu.au3 <-- Here
	|-- yourmenu.exe
	|-- yourinstaller.exe

The AutoIT syntax documentation can be found at `www.autoitscript.com/autoit3/docs`_. Recent versions of the software include a syntax checker (Au3Check).

Note ``your.jpg`` in the file tree above- this is the image referenced in the following line in the menu source code, so adjust as necessary:
``GuiCtrlCreatePic('your.jpg',96,50,157,57)``

To use with Portable GIS the menu source code must be compiled into a standalone executable (.exe). Use Aut2Exe for this (available from your start menu) and supply an icon (.ico) file. 


Installer
---------

The Portable GIS installer is built with `nsis`_. Download the latest version and install on your local machine. 

The Portable GIS installer source file (nsis_installer_xxx.nsi) can be found in the in the `Portable GIS repository`_ and should be placed in the root of your Portable GIS installation, like so:

.. code::

	|-- raw_docs
	|-- usbgis
		|-- apps
		|-- data
		|-- docs
	|-- your.ico
	|-- your.bmp
	|-- portablegis_readme.txt
	|-- yourinstaller.nsi <-- Here
	|-- yourmenu.au3
	|-- yourmenu.exe
	|-- yourinstaller.exe

The nsis syntax documentation can be found at `nsis.sourceforge.net/Docs`_. Ensure that you have a path to an icon (.ico) file and a bitmap image (.bmp) for the installer header. At various places within the installer source file it is necessary to define the version of the software and the uncompressed size of the install- this is used to calculate whether there is enough space on the various drives to do an installation. There are also various file locations throughout the installer- these should be changed to match your setup.

Zip
---

If you wish to build a zip file, then compress the folder ``usbgis``, and the files ``portablegis.exe`` (the menu) and ``portablegis_readme.txt`` into a zip file. You are advised to test it afterwards, using something like ``unzip -t yourzip.zip > error.txt``. Compressing using windows file explorer definitely seems to cause errors when the zip file is tested, though it doesn't necessarily prevent extraction, installation, or use.

**Building the installer takes ages. What can I say, it's a big installation.**

.. _Portable GIS repository: https://gitlab.com/archaeogeek/portable-gis
.. _NSIS: http://nsis.sourceforge.net/Main_Page
.. _nsis.sourceforge.net/Docs: http://nsis.sourceforge.net/Docs
.. _AutoIT: https://www.autoitscript.com
.. _www.autoitscript.com/autoit3/docs: https://www.autoitscript.com/autoit3/docs


