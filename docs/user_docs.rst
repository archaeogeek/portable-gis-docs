====================================
Portable GIS v6.0 User Documentation
====================================

Included software (with versions)
=================================

-   QGIS (3.4.5)
-   PostgreSQL (10.7)
-   PostGIS (2.5)
-   MS4W (4.0.0, including Mapserver 7.4)
-   PgAdmin IV
-   Python (2.7 and 3.7)
-   Loader
-   Geoserver 2.14.4 (with java jdk 1.8)
-   GDAL Python Libraries (1.9)

The following utilities are also included:

-   Firefox (Portable app)
-   Notepad++ (Portable app)
-   Foxit PDF Reader (Portable app)

Links to websites for package-specific documentation

-   QGIS: `<http://www.qgis.org/>`_
-   PostgreSQL: `<http://www.postgresql.org/>`_
-   PostGIS: `<http://postgis.refractions.net/>`_
-   PgAdmin 4: `<http://www.pgadmin.org/>`_
-   MS4W: `<http://www.maptools.org/ms4w>`_
-   Python: `<http://www.python.org/>`_
-   Loader: `<http://github.com/AstunTechnology/Loader>`_
-   Geoserver: `<http://geoserver.org/>`_

Installation
============

Portable GIS is provided as an executable or a zip file. 

**The executable is recommended as it will check the available space on the target drive before beginning the installation.**

Note however, that it will take a significant amount of time to install, as it is limited by the write speed of the target drive (which is generally not all that fast for USB).

**If installing from zip, then ensure that your final extracted file system looks like this or some elements may not work as intended**:

.. code::

    |-- Drive Root
        |-- usbgis
            |-- apps
            |-- data
            |-- docs
                |-- changelog.txt
                |-- license.txt
                |-- pgis_docs.pdf
        |-- portablegis_readme.txt
        |-- portablegis.exe
        |-- Uninstall.exe

Using Portable GIS
==================

If you installed the software from the installer to either your C drive
or a removable drive such as a USB stick you should see a directory
called ``usbgis`` and a file called ``portablegis.exe``. Double-clicking on
portablegis.exe will start the Portable GIS control panel.

**Note that
if installed on a USB stick, packages may take longer to load than you
are used to, due to the slower speed of the USB stick!**

While all care has been taken to minimise incompatibility with
previously installed versions of these packages, the author cannot
guarantee that problems won't occur. Portable GIS is not a “stealth”
installation where no trace will be left on your hard drive, and
similarly some programs may pick up dlls from your system, or values
from your registry.

The Control Panel gives access to the majority of the programs contained
within Portable GIS and allows you to cleanly start and stop the
server-based processes Apache and PostgreSQL. You should only start and
stop these using the control panel to ensure that they start and stop
cleanly.

Note that Apache and PostgreSQL are installed on non-default ports (88
and 5434 respectively) to try and avoid conflict with existing
installations.

Using the Portable GIS Control Panel
====================================

Starting the Control Panel as described above opens up a menu with three
tabs:

Welcome
-------

This tab explains where to go to get help and training.

Desktop Modules
---------------

This tab gives access to all the desk-top packages contained within
Portable GIS, split into sections for Desktop GIS including PgAdmin4
(remember to turn on PostgreSQL from the Server Modules tab before
starting!), Utilities, and Loader.

Server Modules
-------------

This tab allows you to switch on the server-based components of Portable
GIS- Apache, Geoserver and PostgreSQL. Click the ``start`` radio buttons
to start the packages you need. Note that your firewall might try to
block these packages, but they are safe to run. Click the ``stop`` or
``restart`` radio buttons to safely restart or close these packages down.
Portable GIS will close all the web services when the Control Panel is
closed.

Once you have started at least the Apache server, web-based components
can be accessed at `http://localhost:88/ <http://localhost:88/>`_.

Geoserver can be accessed at `http://localhost:8080/geoserver <http://localhost:8080/geoserver>`_.

Utilities
---------

This tab gives you access to the bundled web browser, pdf reader and
text editor.

Portable GIS-Specific Instructions
==================================

Note that these instructions relate only to the specifics of the
Portable GIS setup, and are not comprehensive instructions for using the
software. These can be found at the relevant project websites, listed
above.

QGIS
-----------

QGIS can be started from the control panel Desktop Modules tab.
QGIS includes the GRASS plugin, which is now part of the
processing toolbox. Additional plugins can be installed as usual via the
plugins menu.

Note that the speed at which QGIS starts up is partially
determined by the number of plugins you have loaded. Disable all plugins
that are not required to see a significant improvement in load times.
Note that this information is stored in the windows registry, therefore
it is not possible to set this as part of the default installation.

QGIS may inherit settings from other versions of qgis installed on your computer. If you have trouble starting it, then try deleting any ``*.env`` files from ``[path to usbgis folder]\apps\qgis\bin``.

GDAL/OGR
--------

This option opens a windows command prompt at the correct location for
accessing the ogr and gdal tools with the correct environment variables
set. Please note that these environment variables are set only for the
session initiated using the control panel menu, so the tools may not
work if accessed independently.

Python
------

Python 2.7 is installed from Portable Python, and Python 3.7 is installed as part of MS4W.

If you need to set custom environment variables for your python tools, create a batch file and add it to ``[path to usbgis folder]\apps\scripts``. It will then be accessible from the "Python Scripts" button in the Desktop Modules tab in the control panel. By default this folder contains scripts for editing Loader config using Notepad++ and setting Loader environment variables.


Python 2.7
^^^^^^^^^^

The python 2.7 interpreter can be accessed from the Desktop Modules tab in
the control panel- this will start the interactive python command
prompt. For use in scripts, the python executable should be used- this
can be found at ``[path to usbgis folder]\apps\python27\App\python.exe``

The python 2.7 install includes easy_install for installing additional
python libraries. This can be found in ``[path to usbgis folder]\apps\python27\App\Scripts``. 
Please see the python documentation for usage. 

The module lxml has been included as it is a requirement for Loader to run
(see below).


Python 3.7
^^^^^^^^^^

The python 3.7 interpreter can be accessed from the Desktop Modules tab in
the control panel- this will start the interactive python command
prompt. For use in scripts, the python executable should be used- this
can be found at ``[path to usbgis folder]\apps\ms4w\Python\python.exe``


Loader
------

Loader is an Astun Technology tool that allows the simple loading of KML
and GML into a postgresql database, using Python. It can be found in
``[path to usbgis folder]\apps\loader``. It must be configured before use, using the
text-based config file, loader.config. To edit this file, click "Python Scripts" in the Deesktop Modules tab and run ``edit_loader_config.bat`` to load the config file in Notepad++.

Once configured, return to the "Python Scripts" button in the Desktop Modules tab, and run ``loader_python_vars.bat``. This will set the relevant python environment variables in your command prompt session, change to the loader directory and provide some example syntax on how to run it.

**Loader is currently tested with Python 2.7 only**

Note that Loader will work much more quickly if a temporary working
directory is set up on the C drive rather than on a removable disk. The
location of this directory can be set using the Tools Menu -> "Set
Working Directory" entry. This will also set an environment variable ``%TMPDIR%``
which can be used in the Loader config file as the temporary output
directory. The Tools Menu -> "Empty working directory" entry will remove **any** files from the output directory.


Apache
------

The directory that all web pages should be installed in is as follows: 
``[path to usbgis folder]\apps\ms4w\Apache\htdocs.``

Pages in this directory (and sub folders) will be accessible via
`http://localhost:88/ <http://localhost:88/>`_.
There is some information on the MS4W components at
`http://localhost:88/README_INSTALL.htm <http://localhost:88/README_INSTALL.htm>`_.

To run cgi scripts, please place them in the folder:
``[path to usbgis folder]\apps\ms4w\Apache\cgi-bin``, or edit the apache
configuration files (see below) to give the server permission to execute
scripts from other folders.

The apache configuration files can be found in:
``[driveletter]:\apps\ms4w\Apache\conf``. Please note that changes to this
file may cause apache to stop working.

PostgreSQL
----------

PostgreSQL is set up with the following options: 

 * Username: pgis
 * Password: pgis
 * Port: 3432
 * Host: localhost or 127.0.0.1
 * Spatially enabled template database: template_postgis
 * Data Directory: [path to usbgis folder]\data\PGDATA

The default credentials are set in a pgpass.conf file in ``[path to usbgis folder]\apps\pgsql`` so please edit this if you change the default credentials.

Please create new users if you wish to use this
installation in a public environment. 

PostgreSQL can be administered with the tool
PgAdmin 4 from the control panel, or with the command line tools found
here: ``[path to usbgis folder]\apps\postgresql\bin``.

pgAdmin 4 is set to run on port 5436 and to open the provided portable firefox browser automatically. If this does not happen, visit  ``http://localhost:5436`` in any browser once pgAdmin 4 has started up. If this port conflicts with software running on your computer, change edit ``[path to usbgis folder]\apps\pgsql\pgAdmin 4\web\config_local.py`` in a text editor and change the value of ``DEFAULT_SERVER_PORT``. 

Set up a connection to the database server using the credentials above, or with pgAdmin4 and postgreSQL running, import them using the following command:

``cd [path to usbgis folder]\apps\pgsql\pgAdmin 4\venv\Scripts\
python.exe ..\..\web\setup.py --load-servers ..\..\web\pg-database-server.json``

Note that if you run this command multiple times you will end up with multiple server instances in pgAdmin 4!

Geoserver
---------

Geoserver is set up with the standard initial options, in other words
username: admin, password: geoserver. You may see a java warning when first
loading- please allow java to run.

Mapserver
---------

Mapserver 7.4 is installed in ``[path to usbgis folder]\apps\ms4w\Apache\cgi-bin``. It can be tested at the browser using 
`http://localhost:88/cgi-bin/mapserv.exe <http://localhost:88/cgi-bin/mapserv.exe>`_. Called with no additional
parameters, it will display an error message.

Other Utilities
---------------

PortableGIS contains portable versions of Firefox, a text editor called
Geany, and Foxit PDF Reader. These can be accessed from the relevant
folders of ``[path to usbgis folder]\apps`` but will not launch as the default
applications for web browsing, editing text or reading pdfs unless
specifically set.

License
=======

This product as a whole is distributed under the GNU General Public
License version 3, but it is subordinate to the License Agreements of
the constituent software packages. These may be more or less restrictive
than the GNU GPL. Please ensure that you agree with the terms of all the
licenses before using this software- these can be found in the folders
for the constituent programs.

The full license can be found at: ``[path to usbgis folder]\docs\license.txt``


